![Example fractional cover image](/images/example_bourke.png)

# Advancing Earth Observation Forum 2022 Fractional Cover Workshop
This is a collection of notebooks and scripts to support a workshop on using fractional cover, part of the Advancing Earth Observation Forum, held at the Brisbane Convention & Exhibition Centre, 22 August 2022. The title and abstract for the workshop are below.

## The many faces of fractional cover: demonstrating methods of access and analysis
This will be an interactive workshop, where you will learn about vegetation fractional cover models (un-mixing pixels into photosynthetic vegetation, non-photosynthetic vegetation, and bare ground); how it is used by researchers, government monitoring programs, and industry; and how you can access and analyse pre-processed data. It will involve:
- Presentations from the creators and users of fractional cover data.
- Demonstrations of how to source, create, and analyse fractional cover, such as from the Terrestrial Ecosystem Research Network or Digital Earth Australia.
- The launch of a new (version 3) fractional cover model for Landsat data, and code for developing fractional cover models.
- Sharing of code repositories with examples of how to create, access, and analyse fractional cover using python scripts or Jupyter notebooks.
- Bring your laptop and you can apply new methods to your own interest areas.

## Background
Thanks to a concerted effort by government and university researchers across Australia in collecting consistent field measurements and developing unmixing models, Australian vegetation dynamics can be analysed using fractional cover models that quantify the proportions of photosynthetic vegetation, non-photosynthetic vegetation, and bare ground within satellite pixels. First presented at the 15th Australasian Remote Sensing and Photogrammetry Conference in 2010 by Peter Scarth and others, models for Landsat and MODIS imagery were published in 2015 ([Guerschman et al. 2015](https://doi.org/10.1016/j.rse.2015.01.021)).


## Requirements
To run most of the notebooks and scripts you will need python 3 installed with the following libraries and their dependencies: rios, jupyter, jupyterlab, matplotlib, scikit-learn, git, and curl. One easy way to get a working python 3 environment is to download and install Anaconda.
- Get the version that suits your laptop from https://www.anaconda.com/distribution/
- During installation, accept the default options.
- Once installed, start the program called “Anaconda Prompt (anaconda3)”.
- Create an environment called aeo and install the libraries using this command:

`> conda create -n aeo rios rasterio jupyter jupyterlab matplotlib scikit-learn git curl geopandas`

- Activate the environment, install fast-nnls and start Jupyter Lab:

`> conda activate AEO`

`> pip install git+https://github.com/lostsea/fast-nnls.git`

`> jupyter lab`

To run the fractional cover version 3 notebooks, you need a slightly different set of libraries. This can be done in Anaconda with the following commands.
- Create a new environment called fc3 and install the required libraries from different channels:

`> conda create -n fc3 conda-forge::tensorflow conda-forge::planetary-computer conda-forge::boto3 main::git main::geopandas main::matplotlib main::rasterio main::jupyter main::jupyterlab`

- Activate the environment and install tflite_runtime and fractionalcover3

`> conda activate fc3`

`> pip install tflite_runtime`

`> pip install fractionalcover3`

## Installation of fractionalcover3

The python packages required should all be either available from pypi, or installable from their respective
git repositories (using the `git+hhtps://repository.address/ownner/project` format). If you would prefer
to use a pre-built container, with all the required packages, you can do so using the docker image
[qldrsc/fcv3_workshop](https://hub.docker.com/r/qldrsc/fcv3_workshop). This has python packages and 
other libraries (**WARNING** this is a large container, so would be best to pull it when you have 
good internet). You could use it, for example, like:

```sh
# clone this repository:
git clone https://gitlab.com/jrsrp/themes/cover/aaa_awesome_cover_notebooks.git
cd aaa_awesome_cover_notebooks

# start jupyter lab from the docker image:
docker run --rm -it --name fc_workshop \
    --mount type=bind,source="$(pwd)",target="/workshop" \
    -p 8888:8888 \
    --workdir="/workshop" \
    qldrsc/fcv3_workshop jupyter lab --allow-root --ip 0.0.0.0
```

Then, browse to the address given in the output.

You should be able to open, and run, the example notebooks.

## Support
All code was developed for the workshop and is not part of an ongoing project, and so does not have formal support for further development.

## Contributors
The following contributors helped develop the code and present the workshop, including:
- Adrian Fisher and Andres Sutton (UNSW and JRSRP)
- Robert Denham, Fiona Watson and Rebecca Trevithick (QLD DES and JRSRP)
- Sam Shumack (Climate Friendly)
