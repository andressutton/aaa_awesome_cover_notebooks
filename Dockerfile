FROM qldrsc/devenv_rsc
COPY requirements.txt /tmp
RUN pip3 install --upgrade pip  && \
    pip install wheel -q  && \
    pip install -U setuptools  && \
    pip install -r /tmp/requirements.txt
ENV GDAL_CACHEMAX=1024 \
    GDAL_PAM_ENABLED=OFF
